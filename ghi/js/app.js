function createCard(name, description, pictureUrl, formattedStarts, formattedEnds, cityLocation) {
    return `
      <div class="card shadow">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${cityLocation}</h6>
          <p class="card-text">${description}</p>
          </div>
        </div>
        <div class="card-footer">
        ${formattedStarts}-${formattedEnds}
        </div>
      </div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            showErrorAlert(); // show the error alert

        } else {
            const data = await response.json();

            let col = 0;

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const starts = details.conference.starts;
                    const ends = details.conference.ends;

                    const formattedStarts = new Date(starts).toLocaleDateString();
                    const formattedEnds = new Date(ends).toLocaleDateString();

                    const cityLocation = details.conference.location.name;


                    const html = createCard(title, description, pictureUrl, formattedStarts, formattedEnds, cityLocation);
                    const column = document.querySelector(`#col-${col}`);
                    column.innerHTML += html;
                    col++;
                    if (col > 2) {
                        col = 0;
                    }
                }
            }

        }
    } catch (e) {
        showErrorAlert();// show the error alert
        console.error('It ain\'t working', e.message);

    }

    function showErrorAlert() {
        const errorAlert = document.getElementById('error-alert');
        errorAlert.style.display = 'block'; // displat the error alert
    }




});
